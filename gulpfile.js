var gulp = require('gulp');
var karma = require('karma').server;
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var path = require('path');
var plumber = require('gulp-plumber');
var runSequence = require('run-sequence');
var jshint = require('gulp-jshint');

//var gutil = require('gulp-util');
var minifyCss = require('gulp-minify-css');
var replace = require('gulp-replace-task');
var args    = require('yargs').argv;
var fs      = require('fs');
var sourcemaps = require('gulp-sourcemaps');
var ngAnnotate = require('gulp-ng-annotate');
var inject = require('gulp-inject');


/**
 * File patterns
 **/

// Root directory
var rootDirectory = path.resolve('./');

// Source directory for build process
var sourceDirectory = path.join(rootDirectory, './src');

var sourceFiles = [

  // Make sure module files are handled first
  path.join(sourceDirectory, '/**/*.module.js'),

  // Then add all JavaScript files
  path.join(sourceDirectory, '/**/*.js'),

  '!/**/*.spec.js'
];

var lintFiles = [
  'gulpfile.js',
  // Karma configuration
  'karma-*.conf.js'
].concat(sourceFiles);

gulp.task('build', function() {
  gulp.src(sourceFiles)
      .pipe(plumber())
      .pipe(concat('analytics.js'))
      .pipe(gulp.dest('./dist/'))
      .pipe(uglify())
      .pipe(rename('analytics.min.js'))
      .pipe(gulp.dest('./dist'));
});

gulp.task('ngdocs', [], function () {
    var gulpDocs = require('gulp-ngdocs');

    return gulp.src([
        'src/**/*.constant.js',
        'src/**/*.module.js',
        'src/**/*.js',
        '!src/**/*.spec.js'
        ])
        .pipe(gulpDocs.process())
        .pipe(gulp.dest('dist/docs'));
});

gulp.task('compress', function() {

    //var env = args.env || 'dev';
    //var filename = env + '.json';
    //var settings = JSON.parse(fs.readFileSync('./config/' + filename, 'utf8'));

    return gulp.src([
        'src/**/*.constant.js',
        'src/**/*.module.js',
        'src/**/*.js',
        '!src/**/*.spec.js'
    ])
        .pipe(ngAnnotate())
        .pipe(sourcemaps.init())
        .pipe(concat('analytics.js'))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('dist/'))
        .on('error', function(error){
            console.log(error.toString());
            this.emit('end');
        });
});


gulp.task('watch', function() {
    gulp.watch(sourceFiles, ['compress']);
});

/**
 * Process
 */
gulp.task('process-all', function (done) {
  runSequence('jshint', 'test-src', 'build', done);
});

/**
 * Watch task
 */
gulp.task('watch', function () {

  // Watch JavaScript files
  gulp.watch(sourceFiles, ['process-all']);
});

/**
 * Validate source JavaScript
 */
gulp.task('jshint', function () {
  return gulp.src(lintFiles)
      .pipe(plumber())
      .pipe(jshint())
      .pipe(jshint.reporter('jshint-stylish'))
      .pipe(jshint.reporter('fail'));
});

/**
 * Run test once and exit
 */
gulp.task('test-src', function (done) {
  karma.start({
    configFile: __dirname + '/karma-src.conf.js',
    singleRun: true
  }, done);
});

/**
 * Run test once and exit
 */
gulp.task('test-dist-concatenated', function (done) {
  karma.start({
    configFile: __dirname + '/karma-dist-concatenated.conf.js',
    singleRun: true
  }, done);
});

/**
 * Run test once and exit
 */
gulp.task('test-dist-minified', function (done) {
  karma.start({
    configFile: __dirname + '/karma-dist-minified.conf.js',
    singleRun: true
  }, done);
});

gulp.task('default', function () {
  runSequence('process-all', 'watch');
});
