# senior-network
Bilioteca para tracking em aplicativos utilizando google analytics.


## Como utilizar

### Faça o download da dependência

```
    bower install https://bitbucket.org/ioniclibs/analytics --save
```

### Adicione a biblioteca no index.html do seu projeto.

```
    <script src="lib/senior-network/network.js"></script>
```

### Adicione os plugins

Se estiver utilizando a biblioteca em um projeto Ionic você pode utilizar os comandos ionic state save/ restore para salvar as configurações de ambiente. Dessa maneira todas as plataformas e plugins do projeto podem ser restaurados com um único comando.

```
    ionic plugin add cordova-plugin-google-analytics
```


### Adicione a dependência no módulo

```
    angular.module('starter', ['analytics'])
```


## Preparando o ambiente de desenvolvimento

### Adicionando dependencias (NPM)

```
    npm install
```

### Adicionando dependências (bower)

```
    bower install
```