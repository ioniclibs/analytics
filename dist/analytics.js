angular.module('analytics', []);


angular.module('analytics').factory("GA", GA);

function GA($q, $log) {

    var prefix = null;

    function isAvailable() {

        var available = typeof analytics !== 'undefined';

        if (!available) {
            $log.info("O Google Analytics não está disponível.");
        }

        return available;
    }

    return {
        startTrackerWithId: function (id) {
            var d = $q.defer();

            if (isAvailable()) {
                analytics.startTrackerWithId(id, function (response) {
                    d.resolve(response);
                }, function (error) {
                    d.reject(error);
                });
            } else {
                d.reject("")
            }


            return d.promise;
        },

        setEventPrefix: function(value){
            prefix = value;
        },

        setUserId: function (id) {
            var d = $q.defer();

            if (isAvailable()) {
                analytics.setUserId(id, function (response) {
                    d.resolve(response);
                }, function (error) {
                    d.reject(error);
                });

            } else {
                d.reject();
            }
            return d.promise;
        },

        debugMode: function () {
            var d = $q.defer();

            if (isAvailable()) {
                analytics.debugMode(function (response) {
                    d.resolve(response);
                }, function () {
                    d.reject();
                });
            } else {
                d.reject();
            }

            return d.promise;
        },

        trackView: function (screenName) {

            var d = $q.defer();

            if (isAvailable()) {
                analytics.trackView(screenName, function (response) {
                    d.resolve(response);
                }, function (error) {
                    d.reject(error);
                });
            } else {
                d.reject();
            }

            return d.promise;
        },

        addCustomDimension: function (key, value) {
            var d = $q.defer();

            if (isAvailable()) {
                analytics.addCustomDimension(key, value, function () {
                    d.resolve();
                }, function (error) {
                    d.reject(error);
                });
            } else {
                d.reject();
            }

            return d.promise;
        },

        trackEvent: function (category, action, label, value) {
            var d = $q.defer();

            if (prefix) {
                category = prefix + " - " + category;
            }

            if (isAvailable()) {
                analytics.trackEvent(category, action, label, value, function (response) {
                    d.resolve(response);
                }, function (error) {
                    d.reject(error);
                });
            } else {
                d.reject();
            }

            return d.promise;
        },

        addTransaction: function (transactionId, affiliation, revenue, tax, shipping, currencyCode) {
            var d = $q.defer();

            if (isAvailable()) {
                analytics.addTransaction(transactionId, affiliation, revenue, tax, shipping, currencyCode, function (response) {
                    d.resolve(response);
                }, function (error) {
                    d.reject(error);
                });
            } else {
                d.reject();
            }

            return d.promise;
        },

        addTransactionItem: function (transactionId, name, sku, category, price, quantity, currencyCode) {
            var d = $q.defer();

            if (isAvailable()) {
                analytics.addTransactionItem(transactionId, name, sku, category, price, quantity, currencyCode, function (response) {
                    d.resolve(response);
                }, function (error) {
                    d.reject(error);
                });
            } else {
                d.reject();
            }

            return d.promise;
        }
    }
}
